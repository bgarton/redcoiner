import pyautogui
import pygetwindow as gw
import pydirectinput
import ctypes
import time
import math
import requests #http webhooks
import json
#import datetime
import random

# Get window box
def GetWindowRectFromName(name:str)-> tuple:
    hwnd = ctypes.windll.user32.FindWindowW(0, name)
    rect = ctypes.wintypes.RECT()
    ctypes.windll.user32.GetWindowRect(hwnd, ctypes.pointer(rect))
    # print(hwnd)
    # print(rect)
    return (rect.left, rect.top, rect.right, rect.bottom)
if __name__ == "__main__":
  windowBox = GetWindowRectFromName('Mabinogi')
  print("windowBox:",windowBox)
  pass

### vars
imgDir = 'media/'
targetImg = imgDir + 'target1.png'
targetImg2 = imgDir + 'target2.png'
#targetImg3 = imgDir + 'target3.png'
redcoinImg = imgDir + 'redcoin.png'
loot2Img = imgDir + 'key.png'
lootList = [redcoinImg,loot2Img]
# center screen coords
hx = (windowBox[0] + windowBox[2]) / 2
hy = (windowBox[1] + windowBox[3]) / 2
print("Center of window: x"+str(hx),"y"+str(hy))
confid = 0.85
grey = True
lootTotal = 0
seekFails = 0

## functions

def randomCenterClick():
  pydirectinput.moveTo(int(hx),int(hy))
  x = random.randint(-100,100)
  y = random.randint(-100,100)
  pydirectinput.moveRel(x,y,0.1)
  pydirectinput.click()

def webHook(desc,title="Redcoiner bot says..."):
  url = "https://discordapp.com/api/webhooks/766225897681715292/vO2xhzuAVxehn8z1Db5wsfxoY-ZEPTTvrPkbYGA3pK0RJvOLstec3VeF6WGj45eLNV4p"
  data = {}
  #for all params, see https://discordapp.com/developers/docs/resources/webhook#execute-webhook
  #data["content"] = "message content"
  data["username"] = "RED COINS"

  #leave this out if you dont want an embed
  data["embeds"] = []
  embed = {}
  #for all params, see https://discordapp.com/developers/docs/resources/channel#embed-object
  embed["description"] = desc
  embed["title"] = title
  data["embeds"].append(embed)

  result = requests.post(url, data=json.dumps(data), headers={"Content-Type": "application/json"})

  try:
      result.raise_for_status()
  except requests.exceptions.HTTPError as err:
      print(err)
  #else:
      #print("Payload delivered successfully, code {}.".format(result.status_code))
      #print("Msg:",desc)

# sleep arg in ms
def sleep(v):
  time.sleep( v * .001 ) # ms

def focusMabi():
	win = gw.getWindowsWithTitle('Mabinogi')[0]
	win.activate()
	sleep(10)

# returns array of target box from image path, or a blank array if fail
def findTargets(targetImg):
  global seekFails
  targets = list(pyautogui.locateAllOnScreen(targetImg, confidence=0.80, region=windowBox))
  if (len(targets) < 1):
    i = 0
    timeout = 3 

    while (len(targets) < 1):
      sleep(50)
      i+=1
      if i >= timeout:
        seekFails += 1
        return []
  else:
    return targets

# returns closest target box in array of boxes
def findClosest(targets):
  if len(targets) < 1:
    return
  lst = list()
  for target in targets:
    xLen = target.left - hx
    yLen = target.top - hy
    lst.append(math.sqrt(xLen*xLen + yLen*yLen))
  try:
    closest = targets[lst.index(min(lst))]
  except:
    return None
  #print("Closest target:",closest)
  return closest

def distanceFromCenter(x,y):
  xLen = x - hx
  yLen = y - hy
  dist = math.floor(math.sqrt(xLen*xLen + yLen*yLen))
  #print("Distance from center:",dist)
  return dist

def leftclick(x=None,y=None):
  if x == None and y == None:
    pydirectinput.mouseDown(); time.sleep(.02); pydirectinput.mouseUp()
  else:
    pydirectinput.moveTo(x,y)
    pydirectinput.mouseDown(); time.sleep(.02); pydirectinput.mouseUp()

# hold ctrl and click x,y
def attackTarget(target):
  if target == False or target == None:
    return None
  print("Attacking!")
  pydirectinput.moveTo(target.left, target.top) # move to target
  pydirectinput.moveRel(5,5,0.2)
  #sleep(100) # ensure ctrl is held on the right target...
  pydirectinput.keyDown('ctrl')
  leftclick()
  sleep(50)
  pydirectinput.keyUp('ctrl')
  
# hold alt and click x,y
def clickLoot(x,y):
  pydirectinput.keyDown("alt")
  sleep(50)
  leftclick(x,y)
  sleep(50)
  pydirectinput.keyUp("alt")

#m=metallurgy, l=something, b=bot
def getWaitTime(dist,wait=8000):
  if (dist <= 700 and dist > 400):
    wait = wait * .75
  if (dist <= 400 and dist >= 200):
    wait = wait * .65
  if (dist < 200):
    wait = wait * .35
  if (dist < 100):
    wait = wait * .25
  print("Wait time:",wait)
  return wait

# search and click each item in our master loot list
def findAndClickLoot():
  global lootList
  lootCount=1
  #print("findAndClickLoot() ... lootCount:",lootCount)
  
  #print("To loot:",lootList)
  
  for lootImg in lootList:
    # Keep searching for the current item iteration
    # If not find, search for next item
    # If find, click item and search again for same item
    print("Looking for:",lootImg)
    i=0
    while i < lootCount:
      #print("i:",i,"lootCount:",lootCount)
      pydirectinput.keyDown("alt") # hold alt
      pydirectinput.moveTo(windowBox[0]+100,windowBox[1]+100) # to check loot
      targets = findTargets(lootImg) # search
      if (len(targets) > 0):
        # We found an item
        #print("FOUND")
        if (random.randint(0,100) < 5):
          # Bit of randomness
          print("Random time")
          randomCenterClick()
          sleep(400)
          if (random.randint(0,100) < 50):
            randomCenterClick()
            sleep(250)
        x = targets[0].left
        y = targets[0].top
        leftclick(targets[0].left, targets[0].top+3)
        dist = math.floor(distanceFromCenter(x,y))
        pydirectinput.keyUp("alt")
        sleepv = getWaitTime(dist,5000)
        sleep(sleepv) # get loot timer
      # Else just repeat the loop...
      else:
        pydirectinput.keyUp("alt")
        i+=1 # increment as we've searched and found nothing
    #print("<<Exit while")
  #print("<<Exit for loop")


### main
runCount = 5000 # 1000 = ~3 hours
fightCount = 1

i=0
while i < runCount:
  #fighting
  focusMabi()
  ii=0
  while ii < fightCount:
    closestTarget = findClosest(findTargets(targetImg))
    if closestTarget != None:
      attackTarget(closestTarget)
      sleep(4400)
    else:
      closestTarget = findClosest(findTargets(targetImg2))
      if closestTarget != None:
        attackTarget(closestTarget)
        sleep(4400)
      else:
        print("No target...")
        sleep(100)
    ii+=1
  # now loot
  findAndClickLoot()
  i+=1
exit()