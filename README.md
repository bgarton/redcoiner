# Red coin collector

Made with Python 3.8.6 x32

### Install

- One command:
  `pip install pyautoit pyautogui pydirectinput requests pygetwindow`
- Fill in your webhook url within index.py

### Run

- Open CMD:
  `py index.py`
